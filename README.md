#Container docker database

## Install

### Clone 
* Clone this repo to your local machine using https://gitlab.com/tuflashfood/container.git

Go to the path `../docker-compose.yml` 

Command below to start
```
$ docker-compose up -d (first time)
OR
$ docker-compose start
```

## After docker install success

http://localhost:5050

```
default username : admin@admin.com
default password : 1212312121
```

## Can change data docker
Please feel free to edit `docker-compose.yml` file per your demand.

## Directory Tree
```
├── docker-compose.yml
├── pgadmin
├── postgres
│   └── data [error opening dir]
└── README.md
```
